<?php
namespace app\index\controller;
use app\common\controller\HomeBase;

use app\common\logic\Common as LogicCommon;

class Index extends  HomeBase
{

	private static $commonLogic = null;
	
	public function _initialize()
	{
		parent::_initialize();
		self::$commonLogic = get_sington_object('commonLogic', LogicCommon::class);
	
	}
	
   public function index(){
   	
   	$uid=session('member_info')['id'];
   	
   	if($uid>0){
   		
   		$userdoclistcount = model('doccon')->where(['uid'=>$uid,'status'=>1])->count();
   		
   		$info=self::$commonLogic->getDataInfo('user',['id'=>$uid]);
   		$this->assign('userinfo',$info);
   		$this->assign('userdoccount',$userdoclistcount);
   		
   		
   	}

   	$slideimgs=self::$commonLogic->getDataList('slideimg',['status'=>1]);
   	$this->assign('slideimgs',$slideimgs);
   	
   	
   	
   	$alldoccount = model('doccon')->where(['status'=>1])->count();
   	$alldoccount=$alldoccount;
   	$bit = 9;//产生7位数的数字编号
   	$num_len = strlen($alldoccount);
   	$zero = '';
   	for($i=$num_len; $i<$bit; $i++){
   		$zero .= "0";
   	}
   	$real_num = $zero.$alldoccount;
   	
   	$countarr = str_split($real_num);
   	
   	
   	
   	
   	$this->assign('countarr',$countarr);
   	$this->assign('alldoccount',$alldoccount);
   	
   	
   	$doczdrank=self::$commonLogic->getDataList('doccon',['m.status'=>1,'m.settop'=>1], 'm.*,user.username,user.grades,file.savename,file.ext,user.status as userstatus,user.statusdes', 'm.choice desc,m.create_time desc',false,[['file','m.fileid=file.id','LEFT'],['user','m.uid=user.id','LEFT']],'',2);
   	$this->assign('doczdrank',$doczdrank);
   	
   	//精品文库列表已经改为最新文档
   	$choicedoclist = self::$commonLogic->getDataList('doccon',['status'=>1,'choice'=>1], true, 'create_time desc',false,'','',10);
   
		
   	$this->assign('choicedoclist',$choicedoclist);
   	//最新
   	$newdoclist = self::$commonLogic->getDataList('doccon',['m.status'=>1], 'm.*,user.username,user.grades,file.savename,file.ext,user.status as userstatus,user.statusdes', 'm.create_time desc',false,[['file','m.fileid=file.id','LEFT'],['user','m.uid=user.id','LEFT']],'',10);
	 
   	
   	$this->assign('newdoclist',$newdoclist);
   	//热门文库列表
   	$hotdoclist = self::$commonLogic->getDataList('doccon',['m.status'=>1], 'm.*,user.username,user.grades,file.savename,file.ext,user.status as userstatus,user.statusdes', 'm.view desc,m.create_time desc',false,[['file','m.fileid=file.id','LEFT'],['user','m.uid=user.id','LEFT']],'',10);
   	
   	
   	
   	$this->assign('hotdoclist',$hotdoclist);
   	//公告列表
   	$gglist = self::$commonLogic->getDataList('article',['status'=>1,'tid'=>3], true, 'view desc',false,'','',3);
   	 
   	
   	$this->assign('gglist',$gglist);
   	
   	
   	
   	$yzm_list = parse_config_array('yzm_list');//1\注册2\登录3\忘记密码4\后台登录
   	
   	if(in_array(2, $yzm_list)){
   		 
   		$yzm=1;
   		 
   	}else{
   		 
   		$yzm=0;
   		 
   	}
   	$this->assign('yzm',$yzm);
   	
   	//$groupcatelist=self::$groupcateLogic->getGroupcateList('', true, 'sort desc');
   	
   	$groupcatelist=self::$commonLogic->getDataList('groupcate',['status'=>1],true,'sort desc',false);
   	
   	foreach($groupcatelist as $k => $v){
   		
   		
   		$groupcatelist[$k]['child']=self::$commonLogic->getDataList('doccate',['pid'=>$v['id']],true,'sort desc',false);
   		
   	}
   	$this->assign('catelist',$groupcatelist);
   
   $xscount=model('docxs')->where(['status'=>2,'cnid'=>array('gt',0)])->count();
   $this->assign('xscount',$xscount);
   //悬赏列表
   $xslist = self::$commonLogic->getDataList('docxs',['m.status'=>1], 'm.*,user.username,doccate.name as tidname,groupcate.name as gidname', 'm.create_time desc',false,[['user','m.uid=user.id'],['doccate','m.tid=doccate.id'],['groupcate','m.gid=groupcate.id']],'',6);
    
   
   $this->assign('xslist',$xslist);
   $userxslist = self::$commonLogic->getDataList('docxs',['m.status'=>2,'m.cnid'=>array('gt',0)], 'm.*,doccon.xsid,doccon.uid as duid,doccon.id as did', 'm.update_time desc',false,[['doccon','m.cnid=doccon.id','LEFT']],'',3);
   $this->assign('userxslist',$userxslist);
   
   $zhoutime=time()-7*24*60*60;
   //认证用户文库列表
   $rzuserdoclist = self::$commonLogic->getDataList('doccon',['m.status'=>1,'m.uid'=>array('neq',1)], 'm.uid,sum(m.raty) as sumraty,count(m.id) as dcount,sum(m.down) as sumdown,user.username,user.userhead,user.grades,user.description as udes,user.status as userstatus,user.statusdes', 'dcount desc',false,[['user','m.uid=user.id and user.status=3']],'m.uid',1);
   $zhourzuserdoclist = self::$commonLogic->getDataList('doccon',['m.status'=>1,'m.uid'=>array('neq',1)], 'm.uid,count(m.id) as dcount,sum(m.down) as sumdown,user.username,user.status as userstatus,user.statusdes', 'dcount desc',false,[['user','m.uid=user.id and user.status=3']],'m.uid',5);
   
   $this->assign('zhourzuserdoclist',$zhourzuserdoclist);
   $this->assign('rzuserdoclist',$rzuserdoclist);
 
   
   if($rzuserdoclist){
   	
   	$raty['raty']=array('gt',0);
   	$raty['uid']=$rzuserdoclist[0]['uid'];
   $ratycount=	model('doccon')->where($raty)->count();
   	if($ratycount>0){
   		$rzuserdoclist[0]['raty'] =	round($rzuserdoclist[0]['sumraty']/$ratycount,2);
   	}else{
   		$rzuserdoclist[0]['raty'] =	0;
   	}
  
   $rzuserdoclistarr=getpingfen($rzuserdoclist[0]['raty']);
   $this->assign('rzuserdoclistarr',$rzuserdoclistarr);
   }
  
   
   
   
   
   //非认证用户文库列表
   $norzuserdoclist = self::$commonLogic->getDataList('doccon',['m.status'=>1,'m.uid'=>array('neq',1)], 'm.uid,sum(m.raty) as sumraty,count(m.id) as dcount,sum(m.down) as sumdown,user.username,user.userhead,user.grades,user.description as udes,user.status as userstatus,user.statusdes', 'dcount desc',false,[['user','m.uid=user.id and user.status>0']],'m.uid',1);
   $zhounorzuserdoclist = self::$commonLogic->getDataList('doccon',['m.status'=>1,'m.uid'=>array('neq',1)], 'm.uid,count(m.id) as dcount,sum(m.down) as sumdown,user.username,user.status as userstatus,user.statusdes', 'dcount desc',false,[['user','m.uid=user.id and user.status>0']],'m.uid',5);
  
   if($norzuserdoclist){
   	
   	$raty['raty']=array('gt',0);
   	$raty['uid']=$norzuserdoclist[0]['uid'];
   	$ratycount=	model('doccon')->where($raty)->count();
   	if($ratycount>0){
   		 $norzuserdoclist[0]['raty']=	round($norzuserdoclist[0]['sumraty']/$ratycount,2);
   	}else{
   		 $norzuserdoclist[0]['raty']=	0;
   	}
   	
  
   $norzuserdoclistarr=getpingfen($norzuserdoclist[0]['raty']);
   $this->assign('norzuserdoclistarr',$norzuserdoclistarr);
   }

   
   $this->assign('zhounorzuserdoclist',$zhounorzuserdoclist);
   $this->assign('norzuserdoclist',$norzuserdoclist);
   
   	return $this->fetch();
   	
   }

}
